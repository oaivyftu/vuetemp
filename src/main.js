import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/scss/main.scss'
import 'vuetify/dist/vuetify.min.css'
import 'bootstrap'

Vue.use(Vuetify)

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
