import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/shared/layout'
import Home from '@/views/Home.vue'
import Vuetify from '@/views/Vuetify'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Layout,
      children: [
        {
          path: '',
          name: 'homepage',
          component: Home
        },
        {
          path: 'vuetify',
          name: 'vuetify',
          component: Vuetify
        }
      ]
    }
  ]
})
